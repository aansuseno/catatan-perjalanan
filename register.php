<!DOCTYPE html>
<html>
<head>
	<title>Halaman Daftar</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="kotak-login">
		<h3><center>Halaman Daftar</center></h3>
		<form method="POST" action="register-proses.php">
			<input type="text" name="nik" placeholder="NIK" class="inputan">
			<br>
			<input type="text" name="nama" placeholder="Nama Lengkap" class="inputan">
			<br>
			<div class="kasih-jarak">
				<a href="login.php">Sudah punya akun?</a>
				<input type="submit" class="btn" value="Daftar">
			</div>
		</form>
	</div>
</body>
</html>