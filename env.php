<?php
session_start();
date_default_timezone_set('Asia/Jakarta');

function ke($tujuan)
{
	header('Location: '.$tujuan);
}

function filter()
{
	if (!$_SESSION['login']) {
		ke('login.php');
	}
}


class Model
{
	function buat($namafile, $isi = '')
	{
		file_put_contents($namafile, $isi);
	}

	function tambah($namafile, $databaru)
	{
		$datalama = json_decode(file_get_contents($namafile), true);
		array_push($datalama, $databaru);
		file_put_contents($namafile, json_encode($datalama));
	}

	function ambil($namafile)
	{
		return json_decode(file_get_contents($namafile), true);
	}

	function hapus($namafile, $index)
	{
		$data = json_decode(file_get_contents($namafile), true);
		array_splice($data, $index, 1);
		file_put_contents($namafile, json_encode($data));
	}
}
?>