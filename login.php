<!DOCTYPE html>
<html>
<head>
	<title>Halaman Login</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="kotak-login">
		<h3><center>Halaman Login</center></h3>
		<form method="post" action="login-proses.php">
			<input type="text" name="nik" placeholder="NIK" class="inputan">
			<br>
			<input type="text" name="nama" placeholder="Nama Lengkap" class="inputan">
			<br>
			<div class="kasih-jarak">
				<a href="register.php">Belum punya akun?</a>
				<input type="submit" class="btn" value="Masuk">
			</div>
		</form>
	</div>
</body>
</html>